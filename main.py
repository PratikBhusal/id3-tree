"""
File: main.py - ID3 Decision Tree Algorithm Generator, Printer, and Tester

Author: Pratik Bhusal

Description:
The project implements the greedy ID3 (Iterative Dichotomiser 3) decision tree
algorithm. The program requires 3 command-line arguments in the following
order:
    <training-file> <test-file> <maxTrainingInstances>

The program also prints out a visual representation that follows the format
found in the example given in the assignment description. Finally, the program
also tests the accuracy of the generated decision tree. The precision is up to
2 decimal places as requested by the teaching assistant.

Class Used:
    ID3_Node:
        The class is the basis of the ID3 Decision Tree.

        Class Variables:
            parent: ID3_Node or None
                The parent of the node. If the node is the root of the tree,
                value is None.
            is_leaf: bool
                Flag to indicate if the node is a leaf or not.
            children: list of ID3_Node or []
                The children of the node. If the node is a leaf, it is an empty
                list.
            branch_value: bool or int or str or None
                If node is not the root of the tree, variable indicates the
                value the node was split by it's parent's best attribute to
                split by. If node is the root, value is None.
            attribute: int or str or None
                If the node is not a leaf, variable indicates which attribute
                the node's children is split by. If the node is a leaf, the
                value is None.
            label: bool or int or str or None
                If node is a leaf, variable indicates the class that should be
                associated with the given data. If node is not a leaf, value is
                None.

        Class Methods:
            __str__(self):
                Public method that visually prints out the decision tree based
                on what the TA/Professor's expected format.

            test(self, data_attributes, data_class):
                This function tests the accuracy of the decision tree based on
                the given data's attribute values and class values. The number
                returns a percentage of the tree's accuracy based on the given
                test data.


Functions Used:
    1. split_input_data:
        Splits file steam information and returns them into 3 lists:
            - 1 list to hold instances' attribute values
            - 1 list to hold instances' class values
            - 1 list to hold the names of the attributes

    2. ID3(data_attributes, data_class, attributes, most_common_label,
           parent=None, branch_value=None)
        This function generates the ID3 decision tree. Multiple helper
        functions are used to generate the decision tree.

        See: calculate_entropy()
        See: filter_class_by_attribute()
        See: calculate_expected_entropy()
        See: calculate_best_attribute()
        See: split_data_by_attribute()
    3. calculate_entropy:
        Given the current set of isntance's class values, calculate the entropy
        of the information given.

    4. filter_class_by_attribute(data_attributes: list, data_class: list):
        Filters class values for given instances by attribute value.
        It returns a dictionary of lists where every key-value pair corresponds
        to one possible value for that specific attribute and what instances
        fall into that category respectively.

        See: calculate_entropy()

    5. calculate_expected_entropy(data_attributes: list, data_class: list):
        Given a specific attribute, calculate expected entropy. The function
        returns expected entropy given the specific attribute.

        See: filter_class_by_attribute()
        See: calculate_entropy()

    6. calculate_best_attribute(data_attributes: list, data_class: list):
        Calcuate best attribute to split by based on the expected entropies.
        The function returns the index of the best attribute to split by for
        the current list of attributes to split by.

        See: calculate_expected_entropy()

    7. split_data_by_attribute(data_attributes, data_class, best_attribute):
        Split the current data set into different parts based on the best
        attribute. The data is split by the instance's attribute value for the
        best attribute to split by. The function returns a list of dictionaries
        where each dictionary's key is the attribute value that the data split
        by and the dictionary's value is the information needed for the next
        recursive instance.

        See: ID3()
"""


from collections import Counter
import math
import sys


class ID3_Node:
    """Node instance found in ID3 decision tree.

    Attributes
    ----------
    # associated_tree: ID3_Tree or None
    #     If the node is the root, value is the tree that is associated with
    #     the root. If the node is not the root, value is None.
    parent: ID3_Node or None
        The parent of the node. If the node is the root of the tree, value is
        None.
    is_leaf: bool
        Flag to indicate if the node is a leaf or not.
    children: list of ID3_Node or []
        The children of the node. If the node is a leaf, it is an empty list.
    branch_value: bool or int or str or None
        If node is not the root of the tree, variable indicates the value the
        node was split by it's parent's best attribute to split by. If node is
        the root, value is None.
    attribute: int or str or None
        If the node is not a leaf, variable indicates which attribute the
        node's children is split by. If the node is a leaf, the value is None.
    label: bool or int or str or None
        If node is a leaf, variable indicates the class that should be
        associated with the given data. If node is not a leaf, value is None.

    """

    def __init__(self, parent=None, branch_value=None):
        self.parent = parent
        self.children = []
        self.is_leaf = None
        self.attribtue = None
        self.branch_value = branch_value
        self.label = None

    def _to_str(root, depth: int = 0):
        if not(root):
            return ""

        tree_str = ""
        for child in root.children:
            tree_str += ''.join([
                    "| " * depth,
                    root.attribute if not(root.is_leaf)
                    else root.parent.attribute,
                    ' = ',
                    str(child.branch_value), ' :'
                ]
            )
            tree_str += ''.join(
                ['  ', str(child.label), '\n'] if child.is_leaf
                else ['\n', ID3_Node._to_str(child, depth + 1)]
            )
        return tree_str

    def __str__(self):
        """Generate the visual representation of the ID3.

        Parameters
        ----------
        self: ID3_Node
            The root of the ID3 Tree

        Returns
        -------
        tree_str: str
            Visual Representation of ID3 tree.
        """

        return "Any :  " + str(self.label) + '\n' if self.is_leaf else \
            self._to_str()

    def test(self, data_attributes: list, data_class: list,
             attributes: list) -> float:
        """Calculate acuracy of ID3 decision tree based on the given data set.

        Parameters
        ----------
        root: ID3_Node
            The root of the ID3 tree
        data_attributes: list of list
            list of instances where the ith index corresponds to the ith
            instance's attribute values.
        data_class: list
            list of instances where the ith index corresponds to the ith
            instance's class value.
        attributes: list
            The current list of attributes for the instances.

        Returns
        -------
        accuracy: float
            The percent value of the number of correct classifications by the
            total number of instanes checked.
        """

        num_correct = 0
        instances_checked = 0
        for instances_checked in range(len(data_class)):
            temp_node = self
            while not(temp_node.is_leaf):
                for i in range(len(attributes)):
                    if attributes[i] == temp_node.attribute:
                        temp_node = temp_node.children[
                                    data_attributes[instances_checked][i]]
                        break
            if temp_node.is_leaf:
                num_correct += 1 if \
                    temp_node.label == data_class[instances_checked] else 0

        return ((num_correct * 100) / (instances_checked + 1))


class ID3_Tree:
    class _ID3_Node:
        """Node instance found in ID3 decision tree.

        Attributes
        ----------
        associated_tree: ID3_Tree or None
            If the node is the root, value is the tree that is associated
            with the root. If the node is not the root, value is None.
        parent: ID3_Node or None
            The parent of the node. If the node is the root of the tree, value
            is None.
        is_leaf: bool
            Flag to indicate if the node is a leaf or not.
        children: list of ID3_Node or []
            The children of the node. If the node is a leaf, it is an empty
            list.
        branch_value: bool or int or str or None
            If node is not the root of the tree, variable indicates the value
            the node was split by it's parent's best attribute to split by. If
            node is the root, value is None.
        attribute: int or str or None
            If the node is not a leaf, variable indicates which attribute the
            node's children is split by. If the node is a leaf, the value is
            None.
        label: bool or int or str or None
            If node is a leaf, variable indicates the class that should be
            associated with the given data. If node is not a leaf, value is
            None.
        """

        def __init__(self, parent=None, branch_value=None, tree_ref=None):
            self.parent = parent
            self.children = []
            self.is_leaf = None
            self.attribtue = None
            self.branch_value = branch_value
            self.label = None

            self.associated_tree = None

    def __init__(self):
        self.root = None

    def __str__(self):
        """Generate the visual representation of the ID3.

        Parameters
        ----------
        self: ID3_Node
            The root of the ID3 Tree

        Returns
        -------
        tree_str: str
            Visual Representation of ID3 tree.
        """

        return "Any :  " + str(self.root.label) + '\n' if self.root.is_leaf \
            else ID3_Tree._to_str(self.root)

    def _to_str(current, depth: int = 0):
        if not(current):
            return ""

        tree_str = ""
        for child in current.children:
            tree_str += ''.join([
                    "| " * depth,
                    current.attribute if not(current.is_leaf)
                    else current.parent.attribute,
                    ' = ',
                    str(child.branch_value), ' :'
                ]
            )
            tree_str += ''.join(
                ['  ', str(child.label), '\n'] if child.is_leaf
                else ['\n', ID3_Node._to_str(child, depth + 1)]
            )
        return tree_str

    def split_input_data(data_file, num_instances: int = 0) -> list:
        """Split input by attribute values, class values, and attribute names.

        Parameters
        ----------
        data_file: io.TextIOBase
            The input file to be read and parsed.
        num_instances: int, optional
            The maximum number of instances to be read. If the value <= 0, all
            instances are parsed. Default is to parse all instances.

        Returns
        -------
        input_data: list of list of list
            The input data split into 3 indexies. Index 0 is the attribute
            vaulues for each read instance. Index 1 is the class values for
            each read instance. Index 2 is the name of the attributes.

        Raises
        ------
        ValueError
            If the num_instanes value is greater than the number found in the
            input file.
        """

        input_lines = [
            line_arr for line_arr in
            (list(filter(lambda x:len(x.strip()),
                         line.splitlines()[0].split('\t')))
             for line in data_file.readlines()) if line_arr != []
        ]
        if len(input_lines) < num_instances:
            num_instances = len(input_lines) - 1
        input_data = [
            [
                [
                    int(input_lines[x][y])
                    for y in range(len(input_lines[0]) - 1)
                ]
                for x in range(1, len(input_lines))
            ],
            [int(input_lines[i][-1]) for i in range(1, len(input_lines))],
            [input_lines[0][i] for i in range(len(input_lines[0]) - 1)]
        ] if num_instances <= 0 else [
            [
                [
                    int(input_lines[x][y])
                    for y in range(len(input_lines[0]) - 1)
                ]
                for x in range(1, num_instances + 1)
            ],
            [int(input_lines[i][-1]) for i in range(1, num_instances + 1)],
            [input_lines[0][i] for i in range(len(input_lines[0]) - 1)]
        ]
        return input_data

    def test(self, input_file) -> float:
        input_data = ID3_Tree.split_input_data(input_file)
        return self._test(input_data[0], input_data[1], input_data[2])

    def _test(self, data_attributes: list, data_class: list,
              attributes: list) -> float:
        """Calculate acuracy of ID3 decision tree based on the given data set.

        Parameters
        ----------
        root: ID3_Node
            The root of the ID3 tree
        data_attributes: list of list
            list of instances where the ith index corresponds to the ith
            instance's attribute values.
        data_class: list
            list of instances where the ith index corresponds to the ith
            instance's class value.
        attributes: list
            The current list of attributes for the instances.

        Returns
        -------
        accuracy: float
            The percent value of the number of correct classifications by the
            total number of instanes checked.
        """

        num_correct = 0
        instances_checked = 0
        for instances_checked in range(len(data_class)):
            temp_node = self.root
            while not(temp_node.is_leaf):
                for i in range(len(attributes)):
                    if attributes[i] == temp_node.attribute:
                        temp_node = temp_node.children[
                                    data_attributes[instances_checked][i]]
                        break
            if temp_node.is_leaf:
                num_correct += 1 if \
                    temp_node.label == data_class[instances_checked] else 0

        return ((num_correct * 100) / (instances_checked + 1))

    def build(self, data_file, num_instances) -> ID3_Node:
        input_data = ID3_Tree.split_input_data(data_file, num_instances)
        self.root = ID3(input_data[0], input_data[1], input_data[2],
                        Counter(input_data[1]).most_common(1)[0][0])


def calculate_entropy(data_class: list) -> float:
    """Calculate entropy based on class values.

    Parameters
    ----------
    data_class: list
        The given instances' class values to calculate entropy from.

    Returns
    -------
    entropy: float
        The entropy of the instances' class values.
    """

    entropy = 0
    for i in Counter(data_class).most_common():
        entropy -= (i[1] / len(data_class)) * math.log2(i[1] / len(data_class))
    return entropy


def filter_class_by_attribute(data_attributes: list, data_class: list):
    """Filters class values for given instances by attribute value.

    Parameters
    ----------
    data_attributes: list
        Given the attribute, list of instances where the ith index corresponds
        to the ith instance's attribute value for that attribute.
    data_class: list
        list of instances where the ith index corresponds to the ith instance's
        class value.

    Returns
    -------
    attribute_class_association: dict of list
        Dictionary of lists where every key-value pair corresponds to one
        possible value for that specific attribute and what instances fall
        into that category respectively.
    """

    attribute_class_association = {key: [] for key in set(data_attributes)}

    for i in range(len(data_class)):
        attribute_class_association[data_attributes[i]].append(data_class[i])

    return attribute_class_association


def calculate_expected_entropy(data_attributes: list, data_class: list):
    """Given a specific attribute, calculate expected entropy.

    Parameters
    ----------
    data_attributes: list
        Given the attribute, list of instances where the ith index corresponds
        to the ith instance's attribute value for that attribute.
    data_class: list
        list of instances where the ith index corresponds to the ith instance's
        class value.

    Returns
    -------
    expected_entropy: float
        The expected entropy given the specific attribute.
    """

    expected_entropy = 0
    for _, val in filter_class_by_attribute(data_attributes,
                                            data_class).items():
        expected_entropy += (len(val)/len(data_class)) * calculate_entropy(val)

    return expected_entropy


def calculate_best_attribute(data_attributes: list, data_class: list):
    """Calcuate best attribute to split by based on the expected entropies.

    Parameters
    ----------
    data_attributes: list of list
        list of instances where the ith index corresponds to the ith instance's
        attribute values.
    data_class: list
        list of instances where the ith index corresponds to the ith instance's
        class value.

    Returns
    -------
    best_attribute: int
        The index of the best attribute to split by.
    """

    expected_entropies = [0 for x in range(len(data_attributes[0]))]
    for i in range(len(data_attributes[0])):
        expected_entropies[i] = calculate_expected_entropy(
                            [row[i] for row in data_attributes], data_class)
    return expected_entropies.index(min(expected_entropies))


def split_data_by_attribute(data_attributes: list, data_class: list,
                            attributes: list, best_attribute: int):
    """Split the data set into different parts based on the best attribute.

    Parameters
    ----------
    data_attributes: list of list
        list of instances where the ith index corresponds to the ith instance's
        attribute values.
    data_class: list
        list of instances where the ith index corresponds to the ith instance's
        class value.
    attribute: list
        The current list of attributes for the instances.
    best_attribute: int
        The specific index of the best attribute to split by based on what is
        avaliable in the attribute list

    Returns
    -------
    children_info: list of dict of list
        The data split into different sections based on what the instance's
        value was for the best splitting attribute.
    """

    best_attribute_vals = set(
        [row[best_attribute] for row in data_attributes])
    children_info = {key: [[] for y in range(3)] for key in set(
        best_attribute_vals)}

    for key, _ in children_info.items():
        children_info[key][2] = attributes[:best_attribute] + \
            attributes[best_attribute+1:]

    for i in range(len(data_class)):
        for j in list(best_attribute_vals):
            if data_attributes[i][best_attribute] == j:
                children_info[j][0].append(
                    data_attributes[i][:best_attribute] +
                    data_attributes[i][best_attribute+1:])
                children_info[j][1].append(data_class[i])

    return children_info


def ID3(data_attributes: list, data_class: list, attributes: list,
        most_common_label, parent: ID3_Node = None,
        branch_value=None) -> ID3_Node:
    """Generate Decision Tree based on the ID3 Greedy Decision Tree Alogorithm.

    Parameters
    ----------
    data_attributes: list of list
        list of instances where the ith index corresponds to the ith instance's
        attribute values.
    data_class: list
        list of instances where the ith index corresponds to the ith instance's
        class value.
    attributes: list
        The current list of attributes for the instances.
    most_common_label
    parent: ID3_Node, optional
        The parent of the current node being looked at. If the node is the
        root of the tree, the value is None.

    Returns
    -------
    root: ID3_Node
        The root of the decision tree.
    """

    root = ID3_Node(parent, branch_value)

    if data_class[1:] == data_class[:-1]:
        root.is_leaf = True
        root.label = data_class[0]
        return root
    if any(len(i) == 0 for i in data_attributes):
        root.is_leaf = True
        root.label = Counter(data_class).most_common(1)[0][0]
        return root
    is_data_same = True
    for i in range(len(data_attributes)):
        for j in range(len(data_attributes)):
            if data_attributes[i][:] != data_attributes[j][:]:
                is_data_same = False
                break
    if is_data_same:
        root.is_leaf = True
        root.label = most_common_label if calculate_entropy(data_class) == 1 \
            else Counter(data_class).most_common(1)[0][0]
        return root

    best_attribute = calculate_best_attribute(data_attributes, data_class)
    root.is_leaf = False
    root.attribute = attributes[best_attribute]

    # Split the data based on the most efficient attribute's potential values.
    # Then, recursively create children subtrees.
    for key, value in split_data_by_attribute(data_attributes, data_class,
                                              attributes,
                                              best_attribute).items():

        root.children.append(ID3(value[0], value[1], value[2],
                                 most_common_label, root, key))

    return root


def main():
    training_data_file_name = ""
    test_data_file_name = ""
    num_instances = 0

    if len(sys.argv) != 4:
        raise ValueError(' '.join([
            "Error! Invalid Terminal Argument Count: Program requires",
            "training data file, test data file, and max",
            "number of training instances in that order."]))

    training_data_file_name = sys.argv[1]
    test_data_file_name = sys.argv[2]
    num_instances = int(sys.argv[3])

    tree = ID3_Tree()
    try:
        with open(training_data_file_name, 'r') as training_data_file:
            tree.build(training_data_file, num_instances)
            # training_data_file.seek(0)
    except ValueError as e:
        raise e
    except Exception as e:
        raise e

    tree_str = str(tree)
    print(tree_str)

    test_accuracy = float(0)
    max_num_test_instances = float(0)
    try:
        with open(test_data_file_name, 'r') as test_data_file:
            test_accuracy = tree.test(test_data_file)
    except Exception as e:
        raise e

    try:
        with open(test_data_file_name, 'r') as test_data_file:
            max_num_test_instances = len(ID3_Tree.split_input_data(
                test_data_file)[1])
    except Exception as e:
        raise e
    print("Accuracy on test set (", max_num_test_instances,
          " instances): ",
          format(test_accuracy, '.2f'), '%', sep='')
    print()

    with open("trees/tree2.txt", 'r') as given_tree_file:
        if tree_str == ''.join(given_tree_file.readlines()):
            print("Same tree!")
        else:
            print("Different tree!")

    with open("trees/tree3.txt", 'r') as given_tree_file:
        if tree_str == ''.join(given_tree_file.readlines()):
            print("Same tree!")
        else:
            print("Different tree!")

    # ID3_Tree = ID3(training_input_data[0], training_input_data[1],
    #                training_input_data[2],
    #                Counter(training_input_data[1]).most_common(1)[0][0])
    # tree_str = str(ID3_Tree)
    # print(tree_str)

    # training_accuracy = ID3_Tree.test(training_input_data[0],
    #                                   training_input_data[1],
    #                                   training_input_data[2])
    # test_accuracy = ID3_Tree.test(test_input_data[0],
    #                               test_input_data[1],
    #                               test_input_data[2])
    # print("Accuracy on training set (", len(training_input_data[0]),
    #       " instances): ",
    #       format(training_accuracy, '.2f'), '%\n', sep='')
    # print(num_instances, training_accuracy, test_accuracy, sep='\t')


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e)
